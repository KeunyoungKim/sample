//
//  ViewController.swift
//  FilterSample
//
//  Created by Keun young Kim on 2017. 2. 15..
//  Copyright © 2017년 Keun young Kim. All rights reserved.
//

import UIKit
import MobileCoreServices
import Social
class ViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var originalImage: UIImage?
    var filteredImage: UIImage?

    @IBAction func pick(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            let controller = UIImagePickerController()
            controller.sourceType = .savedPhotosAlbum
            controller.mediaTypes = [kUTTypeImage as String]
            controller.allowsEditing = true
            controller.delegate = self
            
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func showCamera(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let controller = UIImagePickerController()
            controller.sourceType = .camera
            controller.mediaTypes = [kUTTypeImage as String]
            controller.allowsEditing = true
            controller.delegate = self
            
            present(controller, animated: true, completion: nil)
        }
    }
    
    
    
    
    @IBAction func share(_ sender: Any) {
        if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook) {
            let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            vc?.setInitialText("Lorem ipsum dolor sit amet")
            vc?.add(imageView?.image)
            vc?.completionHandler = {
                (result) in
                
            }
            
            show(vc!, sender: nil)
        } else {
            print("not available")
        }
    }
    
    @IBAction func switchImage(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            imageView.image = originalImage
        } else {
            imageView.image = filteredImage
        }
    }
    
    @IBAction func handleGesture(_ sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            imageView.image = originalImage
        default:
            imageView.image = filteredImage
        }
    }
    
    func applyFilter(source: UIImage) {
        guard let cgimg = source.cgImage else {
            print("imageView doesn't have an image!")
            return
        }
        
        let openGLContext = EAGLContext(api: .openGLES2)
        let context = CIContext(eaglContext: openGLContext!)
        
        let coreImage = CIImage(cgImage: cgimg)
        
        let blurFilter = CIFilter(name: "CIBoxBlur")
        blurFilter?.setValue(coreImage, forKey: kCIInputImageKey)
        blurFilter?.setValue(NSNumber(value:20), forKey: kCIInputRadiusKey)
        
        let sepiaFilter = CIFilter(name: "CISepiaTone")
        sepiaFilter?.setValue(blurFilter?.outputImage, forKey: kCIInputImageKey)
        sepiaFilter?.setValue(NSNumber(value: 5), forKey: kCIInputIntensityKey)
        
        if let output = sepiaFilter?.outputImage, let result = context.createCGImage(output, from: output.extent) {
            filteredImage = UIImage(cgImage: result)
            imageView?.image = filteredImage
        }
        
        
        
        
        
        
        
        
        
        
//        
//        let toneFilter = CIFilter(name: "CIToneCurve")
//        toneFilter?.setValue(coreImage, forKey: kCIInputImageKey)
//        toneFilter?.setValue(CIVector(x: 0.0, y: 0.0), forKey: "inputPoint0")
//        toneFilter?.setValue(CIVector(x: 0.3, y: 0.25), forKey: "inputPoint1")
//        toneFilter?.setValue(CIVector(x: 0.48, y: 0.48), forKey: "inputPoint2")
//        toneFilter?.setValue(CIVector(x: 0.64, y: 0.75), forKey: "inputPoint3")
//        toneFilter?.setValue(CIVector(x: 1.0, y: 1.0), forKey: "inputPoint4")
//        
//        let vignetteFilter = CIFilter(name: "CIVignette")
//        vignetteFilter?.setValue(toneFilter?.outputImage, forKey: "inputImage")
//        vignetteFilter?.setValue(NSNumber(value:0.64), forKey: "inputIntensity")
//        vignetteFilter?.setValue(NSNumber(value:1.62), forKey: "inputRadius")
//        
//        let colorPolynomial = CIFilter(name: "CIColorPolynomial")
//        colorPolynomial?.setValue(vignetteFilter?.outputImage, forKey: kCIInputImageKey)
//        colorPolynomial?.setValue(CIVector(x: 0.05, y: 1.0, z: 0.0, w: 0.0), forKey: "inputRedCoefficients")
//        colorPolynomial?.setValue(CIVector(x: 0.0, y: 1.0, z: 0.0, w: 0.0), forKey: "inputGreenCoefficients")
//        colorPolynomial?.setValue(CIVector(x: -0.04, y: 1.0, z: 0.0, w: 0.0), forKey: "inputBlueCoefficients")
//        colorPolynomial?.setValue(CIVector(x: 0.0, y: 1.0, z: 0.0, w: 0.0), forKey: "inputAlphaCoefficients")
//        
//        if let output = colorPolynomial?.outputImage, let result = context.createCGImage(output, from: output.extent) {
//            filteredImage = UIImage(cgImage: result)
//            imageView?.image = filteredImage
//        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        dump(info)
        
        if let type = info[UIImagePickerControllerMediaType] as? String {
            print("type: \(type)")
            
//            if type == kUTTypeMovie as String {
//                let url = info[UIImagePickerControllerMediaURL]
//            } else
            
                if type == kUTTypeImage as String {
                if let metadata = info[UIImagePickerControllerMediaMetadata] as? [String: String] {
                    print("meta: \(metadata)")
                }
                
                if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                    originalImage = image
                    // 1
                    applyFilter(source: image)
                }
                
                if let img = filteredImage {
                    // 2
                    //UIImageWriteToSavedPhotosAlbum(img, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                    //UIImageWriteToSavedPhotosAlbum(img, self, nil, nil)
                }
            }
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
}

